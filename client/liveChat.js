var liveChat = angular.module('liveChat', ['ngRoute' ]);


liveChat.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'index.html',
        controller: 'liveChatController'
      })
      .
      otherwise({
        redirectTo: '/'
      });
  }]);



liveChat.controller('liveChatController', function($scope, $http, $q) {
	 $http.get("/api/ipAddress").success(function (data){
            console.log(data);
            $scope.ipAddress = data;
            // socket.emit('newUser', data);
    });
  var socket = io.connect();
   socket.on('newConnection', function (data) {
//          console.log('recieved user info:'+ data);

          $scope.users = data.map(function (ip){
              return {
                ip : ip
              }

            });
        });
    
    socket.on('newMessage', function (data) {
  //        console.log('recieved user info:'+ data);
          $scope.messages = data;
          $scope.$apply();
  });

   $scope.send = function send() {
          console.log('Sending message:', $scope.text);
          socket.emit('newText',$scope.text);
          //$http.post("/api/newMessage",{text:$scope.text}).success(function (data){
         //   console.log(data);
        //    $scope.messages = data;
       //   });

          $scope.text = '';
        };

});