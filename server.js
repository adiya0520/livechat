var http =require('http');
var express = require('express');
var path = require('path');

var app = express();
var server = app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function (){
	console.log("server started")
});
var io = require('socket.io').listen(server);
app.use(express.static(__dirname + '/client'));

var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

var msgLists = [];
var sockets = [];
var ipLists = [];

app.get('/api/ipAddress',function(req,res){
	res.send(req.ip);
});


// app.post('/api/ipAddress/addNewIp',function(req,res){
// 	ipLists.push(req.ip);
// 	res.send({ipList:ipLists,messages:msgLists});
// });


app.post('/api/newMessage',function(req,res){
	console.log("recieved message:"+ req.body.text);
	msgLists.push({user:req.ip, text:req.param('text')});
	res.send(msgLists);
});

io.on('connection', function(socket){
	console.log('a user has connected:'+socket.handshake.address);
	sockets.push(socket);
	ipLists.push(socket.handshake.address);
	broadcast('newConnection', ipLists);
	broadcast('newMessage', msgLists);

	socket.on('disconnect', function () {
      sockets.splice(sockets.indexOf(socket), 1);
      updateRoster();
    }); 
	socket.on('newText', function (data) {
		msgLists.push({user:socket.handshake.address, text:data, time : new Date() });
		broadcast('newMessage', msgLists);
    });
});

function updateRoster() {
	ipLists = [];
	sockets.forEach(function (socket) {
    	ipLists.push(socket.handshake.address);
  });
  broadcast('newConnection', ipLists);
}

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}